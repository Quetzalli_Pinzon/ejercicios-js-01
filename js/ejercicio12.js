var x = 5 + 7; 
console.log(x); // El resultado es 12

var x = 5 + "7"; 
console.log(x); // El resultado es 57

var x = "5" + 7; 
console.log(x); // El resultado es 57

var x = 5 - 7;  
console.log(x); // El resultado es -2

var x = 5 - "7"; 
console.log(x); // El resultado es -2

var x = "5" - 7; 
console.log(x); // El resultado es -2

var x = 5 - "x";
console.log(x); // El resultado es NaN