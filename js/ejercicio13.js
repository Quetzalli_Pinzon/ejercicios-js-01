var a = 'hello' || '';          // Resultado en consola :  'hello'

var b = '' || [];               //Resultado en consola :   []

var c = '' || undefined;        //Resultado en consola :   undefined

var d = 1 || 5;                 //Resultado en consola :   1

var e = 0 || {};                // Resultado en consola :  {}

var f = 0 || '' || 5;           //Resultado en consola :   5

var g = '' || 'yay' || 'boo';   //Resultado en consola :   'yay'

console.log(a,b,c,d,e,f,g)