var a = 'hello' && '';                  // El resultado es :  ''

var b = '' && [];                       // El resultado es :   ''

var c = undefined && 0;                 // El resultado es :   undefined

var d = 1 && 5;                         // El resultado es :  5

var e = 0 && {};                        // El resultado es :  0

var f = 'hi' && [] && 'done';           // El resultado es :   'done'

var g = 'bye' && undefined && 'adios';  // El resultado es :  undefined

console.log(a,b,c,d,e,f,g)
