var foo = function(val) {
    return val || 'default';
    }

console.log( foo('burger') );  // El resultado en consola es :  burger
console.log( foo(100) );       // El resultado en consola es :  100
console.log( foo([]) );        // El resultado en consola es :  []
console.log( foo(0) );         // El resultado en consola es :  default
console.log( foo(undefined) ); // El resultado en consola es :  default