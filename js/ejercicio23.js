function a(){
    var array = ['a','b','c'];
    console.log(array.join('->'));          // Resultado a->b->c
    console.log(array.join('.'));           // Resultado a.b.c

    console.log('a.b.c'.split('.'));        // Resultado ['a', 'b', 'c']
    console.log('5.4.3.2.1'.split('.'));    // Resultado ['5', '4', '3', '2', '1']
}

a();