function b(){
    var array = [5, 10, 15, 20, 25];

    console.log(Array.isArray(array));      // Resultado true    
    console.log(array.includes(10));        // Resultado true
    console.log(array.includes(10, 2));     // Resultado false
    console.log(array.includes(25));        // Resultado true
    console.log(array.lastIndexOf(10, 0));  // Resultado -1
}

b();