function a(){

    var array = ['a', 'b', 'c', 'd', 'e', 'f'];

    // Modifican el array original
    console.log(array.copyWithin(5, 0, 1));    // Resultado ['a', 'b', 'c', 'd', 'e', 'a']
    console.log(array.copyWithin(3, 0, 3));    // Resultado ['a', 'b', 'c', 'a', 'b', 'c']
    console.log(array.fill('Z', 0, 5));        // Resultado ['Z', 'Z', 'Z', 'Z', 'Z', 'c']
}

a();

function b(){

    var array = ['Alberto', 'Ana', 'Mauricio', 'Bernardo', 'Zoe'];

    // Modifican el array original
    console.log(array.reverse());  // Resultado ['Zoe', 'Bernardo', 'Mauricio', 'Ana', 'Alberto']
    console.log(array.sort());     // Resultado ['Alberto', 'Ana', 'Bernardo', 'Mauricio', 'Zoe']
}

b();

